import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Heading from './components/organisms/heading/heading';
import Main from './components/pages/main/main';
import './App.scss';


function App() {
  return (
    <Router>
      <Heading />
      <Switch>
        <Route component={Main} path="/" exact/>
      </Switch>
    </Router>
  );
};

export default App;
