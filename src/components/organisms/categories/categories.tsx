import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';
import ProgressiveImage from 'react-progressive-graceful-image';

import { CategoriesProps, Category } from './categories-interface';
import './categories.scss';

const Categories:React.FC<CategoriesProps> = ({  categories }) => {
  return (
    <section className="categories">
      {categories.map(RenderCategory)}
    </section>
  );
};

const RenderCategory = (category:Category, index:number):ReactElement => {
  return (
    <article key={index} className="category">
      <Link to={category.path}>
        <ProgressiveImage src={category.image} placeholder={category.placeholder} >
          {(src:string) => <img src={src} alt={category.label} className="category--image" />}
        </ProgressiveImage>
        <p className="category--name">{category.label}</p>
      </Link>
    </article>
  );
}

export default Categories;
