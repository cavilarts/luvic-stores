export interface CategoriesProps {
  categories: Category[]
};

export interface Category {
  path: string;
  label: string;
  image: string;
  placeholder: string;
};