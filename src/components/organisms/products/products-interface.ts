import { ProductProps } from '../../molecules/product/product-interface';
import { ReactNode } from 'react';

export interface ProductsProps {
  products: ProductProps[];
  children: ReactNode;
};