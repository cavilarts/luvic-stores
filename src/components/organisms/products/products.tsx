import React from 'react';

import Product from '../../molecules/product/product';
import { ProductsProps } from './products-interface';
import './products.scss';

const Products:React.FC<ProductsProps> = ({ products, children }) => {
  return (
    <section className="products">
      <div className="products--centered">
        {children}
        <div className="products--list">
          {products.map((product, index) => <Product key={index} id={product.id} mainImage={product.mainImage} altImage={product.altImage} name={product.name} price={product.price} slug={product.slug} size={product.size} />)}
        </div>

      </div>
    </section>
  );
};

export default Products;
