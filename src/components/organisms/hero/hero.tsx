import React from 'react';

import { HeroProps } from './hero-interface';
import './hero.scss';

const Hero:React.FC<HeroProps> = ({children, image}) => {
  const imageStyle = {
    backgroundImage: `url(${image})`
  } as React.CSSProperties;

  return (
    <header className="hero">
      <div className="hero--image" style={imageStyle}></div>
      {children}
    </header>
  );
};

export default Hero;
