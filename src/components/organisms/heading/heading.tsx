import React from 'react';

import Navigation from '../../molecules/navigation/navigation';
import User from '../../molecules/user/user';
import Cart from '../../molecules/cart/cart';

import './heading.scss';

const Heading:React.FC = () => {
  const links = [
    {label: 'Home', url: '/'},
    {label: 'Products', url: '/products'}
  ];

  return (
    <header className="heading">
      <div className="heading__nav">
        <Navigation links={links} />
        <User />
      </div>
      <div className="heading__nav"></div>
      <div className="heading__nav">
        <Cart />
      </div>
    </header>
  );
};



export default Heading;
