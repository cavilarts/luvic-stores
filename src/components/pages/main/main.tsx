import React from 'react'

import Banner from '../../molecules/banner/banner';
import Categories from '../../organisms/categories/categories';
import Products from '../../organisms/products/products';
import Hero from '../../organisms/hero/hero';
import './main.scss';
import { hero, categories, products } from './temp';

const Main:React.FC = () => {
  return (
    <section className="main">
      <Hero image={hero.image} >
        <Banner title="Titulo" subtitle="Subtitulo">
        </Banner>
      </Hero>
      <Categories categories={categories} />
      <Products products={products}>
        <h2>Nuevos Productos</h2>
      </Products>
    </section>
  );
};

export default Main;
