import { ProductProps } from "../../molecules/product/product-interface";

export const hero = {
  image: 'https://via.placeholder.com/3584x1560'
};

export const categories = [{
  path: '/peluches',
  label: 'Peluches',
  image: 'https://via.placeholder.com/475x559',
  placeholder: 'https://via.placeholder.com/175x206'
}, {
  path: '/ropa',
  label: 'Ropa',
  image: 'https://via.placeholder.com/475x559',
  placeholder: 'https://via.placeholder.com/175x206'
}, {
  path: '/bebes',
  label: 'Bebes',
  image: 'https://via.placeholder.com/475x559',
  placeholder: 'https://via.placeholder.com/175x206'
}, {
  path: '/mascotas',
  label: 'Mascotas',
  image: 'https://via.placeholder.com/475x559',
  placeholder: 'https://via.placeholder.com/175x206'
}];

export const products = [{
  id: '1',
  mainImage: 'https://via.placeholder.com/540x805',
  altImage: 'https://via.placeholder.com/540x805',
  name: 'El principito',
  price: 50.000,
  size: 'small',
  slug: 'el-principito',
}, {
  id: '2',
  mainImage: 'https://via.placeholder.com/540x805',
  altImage: 'https://via.placeholder.com/540x805',
  name: 'Combo Tejido Para Bebe',
  price: 50.999,
  size: 'small',
  slug: 'combo-bebe'
}] as ProductProps[];

export const ProductDetails = {
  image: 'https://via.placeholder.com/540x805',
  price: 50.999,
  inStock: true,
  name: 'El principito',
  description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Est ut, corrupti, ea dolorum harum voluptate quisquam debitis omnis quasi eius eum sint consequuntur necessitatibus. Architecto at expedita blanditiis dolorem doloremque recusandae hic eum, placeat reprehenderit ut eos autem quia necessitatibus.',
  reference: '10312'
};
