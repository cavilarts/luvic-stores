import React, { useCallback } from 'react';
import classNames from 'classnames';

import { ButtonProps } from './button-interface';
import './button.scss';

const Button:React.FC<ButtonProps> = ({ children, onClick, type, className }) => {
  const memoizedOnClick = useCallback(
    (event) => {
      if (onClick) {
        onClick(event)
      }
    },
    [onClick],
  )
  const buttonClassName = classNames('button', className);

  return (
    <button type={type} onClick={memoizedOnClick} className={buttonClassName}>
      {children}
    </button>
  );
};

export default Button;
