import { ReactElement, SyntheticEvent } from "react";

export interface ButtonProps {
  children: ReactElement | string;
  className?: string;
  onClick?: (event: Event | SyntheticEvent) => void;
  type: 'button' | 'submit' | 'reset';
}