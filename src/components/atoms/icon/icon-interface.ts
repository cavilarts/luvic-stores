export interface IconProps {
  name: 'home' | 'pencil' | 'image' | 'images' |
  'camera' | 'file-empty' | 'cart' | 'credit-card' |
  'phone' | 'envelop' | 'drawer' | 'drawer-alt' |
  'floppy-disk' | 'spinner' | 'spinner-alt' | 'search' |
  'zoom-in' | 'zoom-out' | 'bin-alt' | 'truck' |
  'menu' | 'checkmark' | 'checkmark-alt' | 'checkbox-checked' |
  'checkbox-unchecked' | 'radio-checked' | 'radio-checked-alt' |
  'radio-unchecked' | 'facebook' | 'instagram' | 'whatsapp' |
  'star-empty' | 'star-full' | 'heart' | 'user' | 'plus' |
  'minus' | 'cross' | 'key' | 'eye';
  size: 'micro' | 'small' | 'medium' | 'large' | 'huge';
}