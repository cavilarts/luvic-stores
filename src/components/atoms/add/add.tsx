import React, { useState } from 'react';

import Button from '../button/button';
import Icon from '../icon/icon';
import { AddProps } from './add-interface';
import './add.scss';

const Add:React.FC = () => {
  const initialState = 1;
  const [state, setstate] = useState(initialState)
  return (
    <div className="add">
      <div className="add--qty">{state}</div>
      <div className="add--actions">
        <Button type="button" onClick={
          () => {
            setstate(state + 1);
          }
        }>
          <Icon name="plus" size="medium"/>
        </Button>
        <Button type="button" onClick={
          () => {
            if (state) {
              setstate(state - 1);
            }
          }
        }>
          <Icon name="minus" size="medium"/>
        </Button>
      </div>
    </div>
  );
};

export default Add;
