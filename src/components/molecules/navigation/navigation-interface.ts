export interface NavigationProps {
  links: LinkInterface[];
}

export interface LinkInterface {
  label: string;
  url: string;
}