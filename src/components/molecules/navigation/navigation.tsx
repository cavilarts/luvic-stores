import React, { ReactElement, useState } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import Button from '../../atoms/button/button';
import Icon from '../../atoms/icon/icon';
import { NavigationProps, LinkInterface } from './navigation-interface';
import './navigation.scss';

const Navigation:React.FC<NavigationProps> = ({ links }) => {
  const initialState = {
    navActive: false
  };
  const [state, setstate] = useState(initialState);
  const navigationClassName = classNames({
    'navigation': !state.navActive,
    'navigation--active': state.navActive
  });
  const overlayClassName = classNames({
    'navigation__overlay': !state.navActive,
    'navigation__overlay--active': state.navActive
  });

  return (
    <>
      <Button type="button" onClick={() => {
          setstate(Object.assign({}, initialState, { navActive: !state.navActive }))
        }}>
          <Icon name="menu" size="medium"/>
        </Button>
      <div className={overlayClassName} onClick={
        () => {
          setstate(Object.assign({}, initialState, {navActive: false}));
        }
      }></div>
      <nav className={navigationClassName}>
        <div className="navigation__heading">
          <p>Productos</p>
          <Button type="button" onClick={
            () => {
              setstate(Object.assign({}, initialState, {navActive: false}));
            }
          }>
            <Icon name="cross" size="medium" />
          </Button>
        </div>
        {links.map(renderLinks)}
      </nav>
    </>
  );
};

const renderLinks = (link:LinkInterface, index:number):ReactElement => {
  return <Link to={link.url} key={index}>{link.label}</Link>;
} 

export default Navigation;
