import React, { ReactElement, useState } from 'react';
import classNames from 'classnames';

import Button from '../../atoms/button/button';
import Icon from '../../atoms/icon/icon';
import { UserProps } from './user-interface';
import './user.scss';

const User:React.FC<UserProps> = () => {
  const initialState = {
    openLogin: false
  }
  const [state, setstate] = useState(initialState);
  const maskClass = classNames('user--mask', { active: state.openLogin });

  return (
    <>
      <Button type="button" onClick={
        () => {
          setstate(Object.assign({}, state, { openLogin: true }));
        }
      }>
        <Icon name="user" size="medium"/>
      </Button>
      <div className="user">
        <div className={maskClass} onClick={
          () => {
            setstate(Object.assign({}, state, { openLogin: false }));
          }
        }></div>
        {RenderModal(state, setstate)}
      </div>
    </>
  );
};

const RenderModal = (state: any, setState:any):ReactElement => {
  const modalClassName = classNames('modal', {active: state.openLogin});

  return (
    <div className={modalClassName}>
      <div className="modal--heading">
        <p>Iniciar Sesion</p>
        <Button type="button" onClick={
          () => {
            setState(Object.assign({}, state, { openLogin: false }));
          }
        }>
          <Icon name="cross" size="medium" />
        </Button>
      </div>
      <div className="modal--form">
        <div className="modal--form__input">
          <label htmlFor="email">Email*:</label>
          <Icon name="user" size="medium"/>
          <input type="email" id="email" />
        </div>
        <div className="modal--form__input">
          <label htmlFor="pass">Contraseña*:</label>
          <Icon name="key" size="medium"/>
          <input type="password" id="pass" />
        </div>
      </div>
      <div className="modal--form__button">
        <Button type="button">Login</Button>
      </div>
      <div className="modal--form__separator">
        <div></div>
        <p>¿No tienes una cuenta?</p>
      </div>
      <div className="modal--form__button--reverse">
        <Button type="button">Registrate</Button>
      </div>
    </div>
  );
}

export default User;
