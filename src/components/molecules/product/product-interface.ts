export interface ProductProps {
  id: string;
  mainImage: string;
  altImage: string;
  name: string;
  price: number;
  size: 'small' | 'medium' | 'large';
  slug: string;
};