import React, { useState } from 'react';
import classNames from 'classnames';
import ProgressiveImage from 'react-progressive-graceful-image';
import { Link } from 'react-router-dom';

import Button from '../../atoms/button/button';
import Icon from '../../atoms/icon/icon';
import ProductDetail from '../product-detail/product-detail';
import { ProductProps } from './product-interface';
import './product.scss';
// TODO remove when real data comes
import { ProductDetails } from '../../pages/main/temp';

const NewProduct:React.FC<ProductProps> = ({ id, mainImage, altImage, name, price, size, slug }) => {
  const initialState = {
    productOpen: false,
    productPosition: 0
  };
  const [state, setstate] = useState(initialState)
  const productClassName = classNames('product', size);
  const styleAltImage = {
    backgroundImage: altImage
  };
  
  return (
    <>
      <article className={productClassName}>
        <Button
          type="button"
          className="product--view"
          onClick={() => {
            setstate(Object.assign({}, state, {
              productOpen: true,
              productPosition: window.scrollY
            })
          );
          }}
        >
          <Icon name="eye" size="medium" />
        </Button>
        <Link to={`/product/${slug}`}>
          <div className="product-image-alt"></div>
          <ProgressiveImage src={mainImage} placeholder={mainImage} >
            {(src:string) => <img src={src} alt={name} className="product--image" />}
          </ProgressiveImage>
        </Link>
        <p className="product--add" style={styleAltImage}>Agregar al carrito</p>
        <Link to={`/product/${slug}`}>
          <p className="product--name">{name}</p>
          <p className="product--price">{price.toFixed(3)}</p>
        </Link>
      </article>
      { ProductDetails && state.productOpen ? RenderProductDetail(id, state.productOpen, ProductDetails, state.productPosition) : null }
    </>
  );
};

const RenderProductDetail = (id:string, active: boolean, details:any, position:number) => {
  return (
    <ProductDetail
        id={id}
        active={active}
        image={details.image}
        price={details.price}
        inStock={details.inStock}
        name={details.name}
        description={details.description}
        reference={details.reference}
        position={position}
      />
  );
} 

export default NewProduct;
