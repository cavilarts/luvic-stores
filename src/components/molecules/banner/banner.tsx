import React from 'react';

import { BannerProps } from './banner-interface';
import './banner.scss';

const Banner:React.FC<BannerProps> = ({children, title, subtitle}) => {
  return (
    <article className="banner">
      {title ? <h2>{title}</h2> : null}
      {subtitle ? <p>{subtitle}</p> : null}
      {children}
    </article>
  );
};

export default Banner;