import { ReactNode } from "react";

export interface BannerProps {
  children: ReactNode;
  title?: string;
  subtitle?: string;
};