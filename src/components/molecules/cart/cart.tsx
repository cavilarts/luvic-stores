import React, { ReactElement, useState } from 'react';
import classNames from 'classnames';

import Button from '../../atoms/button/button';
import Icon from '../../atoms/icon/icon';
import { CartProps, CartState } from './cart-interface';
import './cart.scss';

const Cart:React.FC<CartProps> = () => {
  const initialState = {
    cartOpen: false,
    products: []
  };
  const [state, setstate] = useState(initialState);
  const cartMaskClassName = classNames('cart--mask', { active: state.cartOpen });
  return (
    <>
      <Button type="button" onClick={
        () => {
          setstate(Object.assign({}, state, {cartOpen: true}));
        }
      }>
        <Icon name="cart" size="medium"/>
      </Button>
      <div className={cartMaskClassName} onClick={
        () => {
          setstate(Object.assign({}, state, {cartOpen: false}));
        }
      }/>
      {renderCart(state, setstate)}
    </>
  );
};

const renderCart = (state:CartState, setState: any): ReactElement => {
  const cartClassName = classNames('cart', {active: state.cartOpen});

  return(
    <section className={cartClassName}>
      <div className="cart--heading">
        <p>CARRITO</p>
        <Button type="button" onClick={
          () => {
            setState(Object.assign({}, state, {cartOpen: false}));
          }
        }>
          <Icon name="cross" size="medium"/>
        </Button>
      </div>
      <div className="cart--content">
        {state.products.length ? state.products.map(renderProducts) : renderEmptyCart()}
      </div>
    </section>
  );
};

const renderProducts = (): ReactElement => {
  return (
    <article></article>
  );
};

const renderEmptyCart = (): ReactElement => {
  return (
    <article></article>
  );
};

export default Cart;
