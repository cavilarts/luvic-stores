import React from 'react';
import ProgressiveImage from 'react-progressive-graceful-image';
import classNames from 'classnames';

import Button from '../../atoms/button/button';
import Icon from '../../atoms/icon/icon';
import { ProductDetailProps } from './product-detail-interface';
import './product-detail.scss';

const ProductDetail:React.FC<ProductDetailProps> = ({ 
    active,
    image,
    price,
    inStock,
    name,
    description,
    reference,
    position
  }) => {
  const maskClassNames = classNames('product-detail--mask', {active: active});
  const productDetailClassNames = classNames('product-detail', {active: active});
  const productDetailStyle = {
    top: `${position}px`
  };

  return (
    <>
      <div className={maskClassNames}></div>
      <section className={productDetailClassNames} style={productDetailStyle}>
        <Button type="button" className="product-detail--close">
          <Icon name="cross" size="medium"></Icon>
        </Button>
        <ProgressiveImage src={image} placeholder={image}>
          {(src:string) => <img src={src} alt={name} className="product-detail--image" />}
        </ProgressiveImage>
        <article className="product-detail--resume">
          <p className="product-detail--name">{name}</p>
          <p className="product-detail--pricing">
            <span className="product-detail--price">{price}</span>
            <span className="product-detail--stock">{inStock ? 'En Stock': 'No disponible'}</span>
          </p>
          <div className="product-detail--separator"></div>
          <p className="product-detail--description">{description}</p>
          <p className="product-detail--ref">Ref: {reference}</p>
          <Button className="product-detail--cta" type="button">Agregar Al Carrito</Button>
        </article>
      </section>
    </>
  );
};

export default ProductDetail;
