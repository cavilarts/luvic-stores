export interface ProductDetailProps {
  id: string;
  active: boolean;
  image: string;
  price: number;
  inStock: boolean;
  name: string;
  description: string;
  reference: string;
  position: number;
};